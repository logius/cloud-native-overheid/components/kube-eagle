# Kube Eagle

Metrics exporter that links resource requests and limits with actual resource usage at various levels, from container to cluster.

Source repo: https://github.com/cloudworkz/kube-eagle

## Component
The component consists of:
* a Helm chart that deploys a single deployment (`kube-eagle`) in its own namespace
* a ServiceMonitor that makes a pre-deployed Prometheus instance scrape Kube Eagle's metrics

The chart can also deploy a ServiceMonitor but the CNO component deploys its own ServiceMonitor instead for more flexibility.

## Requirements
Kube Eagle requires [Metrics Server](https://github.com/kubernetes-incubator/metrics-server) to be installed. Most Kubernetes distributions, including Openshift already contain Metrics Server.

## Manual steps
In order to benefit from the metrics of Kube Eagle, import dashboard ID 9871 in your Grafana instance.
