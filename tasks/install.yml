---
- name: Set OpenShift security context constraints
  include: scc.yml
  when: config.system.k8s_distribution == 'openshift'

- name: Set default chart values
  set_fact:
    default_chart_values:
      image:
        repository: quay.io/google-cloud-tools/kube-eagle
        tag: "{{ config.kube_eagle.app_version }}"
  no_log: true

- name: Set combined chart values
  set_fact:
    chart_values: "{{ default_chart_values | combine(config.kube_eagle.extra_values | default({})) }}"
  no_log: true

- name: Helm install Kube Eagle
  helm3:
    chart: "{{ charts.kube_eagle }}"
    state: present
    release: kube-eagle
    namespace: "{{ config.kube_eagle.namespace }}"
    wait: true
    timeout: "10m0s"
    create_namespace: true
    values: "{{ chart_values }}"

  async: 900  # Max 15 minutes for the install
  poll: 0
  register: kube_eagle_installer

- name: Wait for the Kube Eagle install to be finished
  async_status:
    jid: "{{ kube_eagle_installer.ansible_job_id }}"
  register: job_result
  until: job_result.finished
  retries: 100
  delay: 10

- name: Deploy ServiceMonitor
  k8s:
    state: present
    definition: "{{ lookup('template', 'kube-eagle-service-monitor.yml') | from_yaml }}"
